# EXERCISE1 - TRAVEL AGENCY #ffff

The aim of these tasks is to use git knowledge in practice

### TASK 1 ###

* create new hotel in italy following steps:
    * create italy directory in travelagency/hotels directory
    * create json file with hotel's location
    * add hotel json object in given location

### TASK 2 ###

* create new attractions(sighseeing,relax) for the same location
> according to usa example

### TASK 3 ###

* create new user with your data


### TASK 4 ###

* create new trip to Italy based on existing hotels and attractions

### TASK 5 ###

* create new international trip selected by you
    * trip length: 5 days
    * at least 2 countries
    * at least 3 locations, 3 hotels
    * at least one new coutry

