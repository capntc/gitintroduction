# EXERCISE2 - IKEA SHOP #

The aim of these tasks is to use git knowledge in practice

Introduction
You are working in agency that helps people make them home looks better.
Mr & Mrs Kapgeminiusz need help of our agency.
We need to select some fancy furniture from IKEA store :)
Each one of you need to take care of one aspect of room decoration (such as accesories, furnitures, devices etc)
As a final result we need to provide a .json file with a complete list of furniture, they will use in their new home. 

### TASK 1 ###

* Add 3 items according to your directory`s name to your copy of furniture.json, commit and push changes to your branch.
> Example
>{
>	"furniture": [{
>			"name": "POÄNG",
>			"url": "http://www.ikea.com/pl/pl/catalog/products/S69090357/",
>			"price": 249.00
>		}
>...
>	]
>}

    
### TASK 2 ###

* Add your furnitures to your room's furniture .json file, commit and push changes to your`s team branch

### TASK 3 ###

* Add new property "description" in json file. Fill description from ikea website. Dont forget to add it to team's .json file. Commit, and push. 
> Example
>{
>	"furniture": [{
>			"name": "POÄNG",
>			"url": "http://www.ikea.com/pl/pl/catalog/products/S69090357/",
>			"price": 249.00,
>			"description": "This is sample description"
>		}
>...
>	]
>}

### TASK 4 ###

In team, fill the main .json file, commit, push and create pull request to create final .json file

