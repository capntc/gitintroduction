# README #

There are basic informations about GIT training repository

### What is this repository for? ###

* GIT training on LevelUp project
* 1.0

### Requirements ###

* installed GIT: [download](https://git-scm.com/downloads)
+ read JSON introduction:
	* [JSON descirption](https://www.tutorialspoint.com/json/)
	* [JSON tutorial](https://www.codecademy.com/courses/javascript-beginner-en-xTAfX/0/1)
* installed SourceTree: [download](https://www.sourcetreeapp.com/)
* installed KDIFF [download](https://sourceforge.net/projects/kdiff3/files/)

### How do I get set up? ###
* clone repository using Git Console or Source Tree

### Contribution guidelines ###

Perform following steps:

> git checkout develop
>
> git pull origin develop
>
> git checkout -b branchName
>
> > add your code
>
> git add (.|filename)
>
> git commit -m "commit message"
>
> git push origin branchName
>
> > create pull request from branchName to develop
