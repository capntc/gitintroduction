# Comit and push changes

## Context

It is a good idea to save your changes as often as possible. In git you can save changes only locally and them push this changes to remote repository. Don`t be affraid to commit or push code which is not perfect. There is no wrong with that. But remember do this only on your local branch. If you want to merge/push to develop branch the code should be ready and you need to be proud of that code.

## Steps

1. identify changes you want to commit
2. stage those changes
3. commit changes
4. push changes

## Example

###### Assumptions:

* I am on `feature/michal-git-book` branch
* I have many modified files but I want to commit only some of them

### Console

1. type `git status` to see modified/new files on current branch:

```
$ git status
On branch feature/michal-git-book
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   README/commit.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   README/reverting.md
        modified:   README/push.md
```

2. Then you need to decide which files you want to commit. You can do it by typing:

```
git add <file-name>
for example:
git add README/commit.md
To stage all files:
git add .
```

3. Next step is to commit staged changes

```
$ git commit -m "Add tutorial for commit"
```

> Remember to always add commit message

> If you dont add -m switch vim editor will open. Please follow instructions described [here](https://stackoverflow.com/questions/9171356/how-do-i-exit-from-the-text-window-in-git) to exit vim editor.

4. All commited changes needs to be pushed so they can be visible to others members of the project. To push changes just type

```
$ git push
```

> You can get couple of errors here. I will describe two most popular.

1. No upstream branch

```
$ git push
fatal: The current branch feature/michal-git-book has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin feature/michal-git-book
```

This means that you have a branch locally that has no corresponding branch remotly. You need to create remote branch and set it to track your local branch. You can do it in one single step.

```
git push --set-upstream origin <branch name>
```

2. Failed to push some refs

```
$ git push
To https://bitbucket.org/capntc/gitintroduction.git
 ! [rejected]        feature/michal-git-book -> feature/michal-git-book (fetch first)
error: failed to push some refs to 'https://bitbucket.org/capntc/gitintroduction.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

That means your branch is not up to date and you need to pull changes before pushing anything to that branch.

```
git pull
```

merge confilicts if any and then

```
git push
```

### SourceTree
