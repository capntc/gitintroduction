# Starting work on new feature #

> you have to start new task/feature

## Context ##
When we want to start new task we we'll need to create new branch

## Steps ##
1. commit/discard your actual changes
2. switch into <team-working-branch> (`develop` in most cases)
3. make sure that you are up-to-date 
4. create new branch
5. switch into new branch 
6. make sure that you are in new branch

### Example ###

###### Assumptions: ######
- I am on random branch

### Console ###
1) To check your actual status of changes, type:
```
git status
```

2) You have to decide if uncommitted changes should be committed or discarded:

- to commit, check [commit and push](./commit_and_push.md) 
- to discard, type: 
```
git reset --hard
```

3) To switch into <team-working-branch>, type:
```
git checkout <team-working-branch>
```

5) To pull newest changes on actual branch, type:
```
git pull
```

6) To create new branch, type:
```
git branch feature/newFeature
``` 

7) To switch into new branch, type:
```
git checkout feature/newFeature
```

8) To make sure that you are in good branch, type:
```
git status
```

After successful creating and switching you should see similar output:
```
$ git status
On branch feature/newFeature
nothing to commit, working tree clean
```

### Hint ###
You can create and switch into new branch, using one command:
```
git checkout -b 'feature/newFeature'
```

### SourceTree ###