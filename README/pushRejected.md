#  Push rejected #

> found committer name '' but expected ''
>
> found committer email '' but expected ''

## Context ##
The error occurs when other committer (or the same committer with other credentials) tries to push own commits to someone's branch on remote.
Resolving this issue can be made by resetting failing commit(s), updating user credentials and pushing changes again.

## Steps ##
1. reset changes from failing commit
2. all changes should be in `unstaged` mode
3. update git user credentials according to demand
4. make sure that appropriate credentials are set
5. you can commit changes again in normal mode

Repeat these steps up to successful push message.

## Example ##

Example error message met in console:
```
remote:   (c).-.(c)    (c).-.(c)    (c).-.(c)    (c).-.(c)    (c).-.(c)
remote:    / ._. \      / ._. \      / ._. \      / ._. \      / ._. \
remote:  __\( Y )/__  __\( Y )/__  __\( Y )/__  __\( Y )/__  __\( Y )/__
remote: (_.-/'-'\-._)(_.-/'-'\-._)(_.-/'-'\-._)(_.-/'-'\-._)(_.-/'-'\-._)
remote:    || E ||      || R ||      || R ||      || O ||      || R ||
remote:  _.' `-' '._  _.' `-' '._  _.' `-' '._  _.' `-' '._  _.' `-' '.
remote: (.-./`-'\.-.)(.-./`-`\.-.)(.-./`-`\.-.)(.-./`-'\.-.)(.-./`-`\.-.)
remote:  `-'     `-'  `-'     `-'  `-'     `-'  `-'     `-'  `-'     `-'
remote:
remote: Push rejected.
remote:
remote: refs/heads/feature/feature1: 49803a7581e57324e85a2e19aeea41cfdeb46701: found committer email 'bob.smith@domain.com' but expected 'Alice.Murphy@domain.com' or 'alice.murphy@domain.com'
remote:
remote: refs/heads/feature/feature1: 49803a7581e57324e85a2e19aeea41cfdeb46701: found committer name 'Smith, Bob' but expected 'murphy, alice' or some combination of 'Murphy, Alice'
```

###### Assumptions: ######
- I am on `feature/feature1` branch
- I am user with credentials:
    - name `bob.smith@domain.com`
    - email  `Smith, Bob`
- I have commit with commit-id 49803a7581e57324e85a2e19aeea41cfdeb46701 and before it commit-id 2b3715893afe2f4b45fe959f2ac10d13640a3f26

### Console ###

1. To reset changes from failing commit with commit-id 49803a7581e57324e85a2e19aeea41cfdeb46701, type commit-id directly before it:
```
git reset 2b3715893afe2f4b45fe959f2ac10d13640a3f26
```
2. To make sure if all changes are in unstaged state, type:
```
git status
```
3. To change committer name or email, type:
```
git config user.name "Murphy, Alice"
git config user.email "alice.murphy@domain.com"
```
4. To check committer name or email, type:
```
git config user.name
git config user.email
```
5. To commit all changes, type:
```
git add
git commit -m "change committer credentials"
git push origin feature/feature1
```

After successful pushing you should see following output:
```
remote:
remote: Create pull request for feature/feature1:
remote:   <git-repo-url>/compare/commits?sourceBranch=refs/heads/feature/feature1
remote:
To <git-repo-url>
   4d27ef0d0..690c01f2a  feature/feature1 -> feature/feature1

```

### SourceTree ###

1. To reset changes from failing commit with commit-id 49803a7581e57324e85a2e19aeea41cfdeb46701, type:

2. To make sure if all changes are in unstaged state, type:

3. To change committer name or email, type:

4. To check committer name or email, type:

5. To commit all changes, type:


After successful pushing you should see following output:

### Tip ###
To check log history with commit-id, type
```
git log
```