# Sync with develop/feature branch

## Context

Sometimes you may want to use other work. For example you would like to see in your code what others already did. Or you want to get latest develop version to
start working on new feature. In that case you need to sync with desired branch. Sync means pulling changes from other branch and merge it with your code.

## Steps

1. identify branches you want to sync
2. switch to target branch
3. pull changes from target branch
4. switch back to source branch
5. merge changes from target branch into source branch
6. resolve conflicts (if any)

## Example

###### Assumptions:

* I am on `feature/michal-git-book` branch and would like to sync with `develop` branch.
* `feature/michal-git-book` is source branch, and `develop` is target branch

### Console

1. commit or stash any existing changes
2. type `git checkout develop` to switch to develop branch:

```
$ git checkout develop
Switched to a new branch 'develop'
Branch 'develop' set up to track remote branch 'develop' from 'origin'.
```

3. Then you need to get all changes from remote in order to have them locally. Just pull changes.

```
$ git pull
remote: Counting objects: 28, done.
remote: Compressing objects: 100% (28/28), done.
remote: Total 28 (delta 16), reused 0 (delta 0)
Unpacking objects: 100% (28/28), done.
From https://bitbucket.org/capntc/gitintroduction
   372aa47..c8fc6bf  feature/README_Pawel -> origin/feature/README_Pawel
Already up to date.
```

4. Switch back to source branch

```
$ git checkout feature/michal-git-book
Switched to branch 'feature/michal-git-book'
```

5. Now it`s time to merge all changes from`develop`

```
$ git merge develop -m "I want all develop changeeeees"
Merge made by the 'recursive' strategy.
 README/cloning.md   | 74 +++++++++++++++++++++++++++++++++++++++++++++++++++++
 README/newBranch.md | 65 ++++++++++++++++++++++++++++++++++++++++++++++
 README/switching.md | 55 +++++++++++++++++++++++++++++++++++++++
 3 files changed, 194 insertions(+)
 create mode 100644 README/cloning.md
 create mode 100644 README/newBranch.md
 create mode 100644 README/switching.md
```

6. Sometimes at this time you can get an error with conflicting changes. You need to resolve all conflicts before going further.

And that`s it. Now you are proud owner of latest develop code.

### SourceTree
